<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	function headers_txt_2_array($hd) {
		$headers_lines = preg_split('/(\\r\\n)|\\r|\\n/', $hd);
		return $headers_lines;
	}

	function handleHeaderLine( $curl, $header_line ) {
		header($header_line);
		return strlen($header_line);
	}

    define('GITLAB_HOME', 'https://gitlab.com');
    define('PROJ_HOME', GITLAB_HOME . '/jorgepynho/phpcurltestpage');

	$request_method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
	$response = '';
	$error_message = '';

	$req_method = '';
	$req_url = '';
	$req_data = '';
	$req_user_agent = 'cURL test page; ' . PROJ_HOME;
	$req_header_content_type = '';
	$headers_txt = '';
	$response_headers = array();

	$request_methods_list = array('GET', 'POST', 'PUT', 'DELETE', 'HEAD');

	if ($request_method == 'POST') {
		$req_method = filter_input(INPUT_POST, 'method');
		$req_url = filter_input(INPUT_POST, 'url');
		$req_data = filter_input(INPUT_POST, 'data');
		$req_user_agent = filter_input(INPUT_POST, 'user_agent');
		$req_header_content_type = filter_input(INPUT_POST, 'header_content_type');
		$headers_txt = filter_input(INPUT_POST, 'headers_txt');
		$full_page_output = (bool) filter_input(INPUT_POST, 'full_page_output');

		$ch = curl_init($req_url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);

		if ($full_page_output) {
			curl_setopt($ch, CURLOPT_HEADERFUNCTION, 'handleHeaderLine');
		}

		if ($req_data) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req_data);
		}

		if ($req_user_agent) {
			curl_setopt($ch, CURLOPT_USERAGENT, $req_user_agent);
		}

		$headers = array();

		if ($req_header_content_type) {
			$headers['Content-Type'] = $req_header_content_type;
		}

		if ($headers_txt) {
			$req_headers = headers_txt_2_array($headers_txt);

			$headers = array_merge($headers, $req_headers);
		}

		if ($headers) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($ch);

		if (curl_errno($ch)) {
			$error_message = 'Curl error: ' . curl_error($ch);
		}

		curl_close($ch);

		if ($full_page_output) {
			echo $response;
			exit;
		}
	}
?><!DOCTYPE html>
<html>
<head>
	<title>cURL test page</title>
	<style>
		* {
			font-family: "Trebuchet MS", Helvetica, sans-serif;
		}

		form input[type='text'] {
			width: 500px;
		}

		footer {
			text-align: right;
		}

		.error_msg {
            color: red;
            font-weight: bold;
		}
	</style>
	<script>
		function toggle_data_field() {
			fdf = document.getElementById('form_data_field');
			display_status = fdf.style.display;

			if (display_status == 'none') {
				fdf.style.display = 'inline';
				return;
			}

			fdf.style.display = 'none';
		}

		function doSubmit(f) {

			if (!f.url.value) return false;

			var saved_requests = localStorage.getItem("saved_requests");

			if (!saved_requests || saved_requests.length < 1) {
				saved_requests = new Array();
			} else {
				saved_requests = JSON.parse(saved_requests);
			}

			saved_requests.push(f.url.value);

			localStorage.setItem("saved_requests", JSON.stringify(saved_requests));

			return true;
		}

		function set_request_url(u) {
			f = document.getElementById('curl_form');
			f.url.value = u;
		}

		function list_previous_urls() {
			var saved_requests_json = localStorage.getItem("saved_requests");
			var saved_requests;

			if (saved_requests_json) {
				saved_requests = JSON.parse(saved_requests_json);

				var list_urls = '';

				saved_requests.forEach(function(e) {
					list_urls += '<li><a href="#" onclick="set_request_url(\'' + e + '\')">' + e + '</a></li>';
				});

				ul = document.getElementById('previous-list');

				ul.innerHTML = list_urls;
			}
		}

		function remove_ua() {
			f = document.getElementById('curl_form');
			f.user_agent.value = '';
		}
	</script>
</head>
<body>
	<header>
		<h1><a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>">cURL test page</a></h1>
	</header>
	<hr>
	<section>
	<fieldset>
		<legend>Previous</legend>
		<ul id="previous-list">
		</ul>
		<script>
			list_previous_urls();
		</script>
	</fieldset>
	<br>
	<form id="curl_form" action="" method="post" onsubmit="return doSubmit(this);">
		Method <select name="method">
			<?php foreach($request_methods_list as $rq) { ?>
				<option value="<?php echo $rq; ?>" <?php if ($rq == $req_method) {echo 'selected="selected"';} ?>><?php echo $rq; ?></option>
			<?php } ?>
		</select>
		<br><br>

		URL <input type='text' name="url" autofocus="autofocus" value="<?php echo $req_url; ?>">
		<br><br>

		<fieldset>
			<legend>Headers</legend>

			Content-Type <input type='text' name="header_content_type" value="<?php echo $req_header_content_type; ?>">
			<br><br>
			User Agent <input type='text' name="user_agent" value="<?php echo $req_user_agent; ?>">&nbsp;<a href="#" onclick="return remove_ua();">remove</a>
			<br><br>
			more headers<br>
			<textarea name="headers_txt" cols="50"><?php echo $headers_txt; ?></textarea>
		</fieldset>

		<br><br>

		data <a href="#" onclick="toggle_data_field();">show/hide</a><br>
		<textarea id="form_data_field" name="data" rows="3" cols="100"><?php echo $req_data; ?></textarea>
		<br><br>

		<input type="submit" value="do request"><br>

		<label>
			<input type="checkbox" name="full_page_output" value="1">full page output
		</label>
	</form>
	</section>
	<hr>
	<section>
		<h2>response</h2>
        <?php if ($error_message) { ?>
        <div class="error_msg"><?=$error_message ?></div>
        <?php } ?>
		<textarea rows="20" cols="100"><?php echo $response; ?></textarea>
	</section>
	<hr>
	<footer>
		<p>&copy; JPy 2015</p>
	</footer>
</body>
</html>
